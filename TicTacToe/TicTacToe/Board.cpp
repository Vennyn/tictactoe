#include <iostream>
#include "Board.h"

Board::Board() {
	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			this->m_board[i][j] = '-';
		}
	}
}

bool Board::checkMove(int row, int col){
	if (this->m_board[row][col] != '-')return false;
	else return true;
}

void Board::makeMove(int row, int col, char player){
	this->m_board[row][col] = player;
}

void Board::printBoard(){
	for (int i = 0; i < 3; ++i) {
		for (int j = 0; j < 3; ++j)
		{
			printf("%c ", this->m_board[i][j]);
		}
		printf("\n");
	}
}