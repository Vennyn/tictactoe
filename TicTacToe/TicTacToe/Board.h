#pragma once
class Board {
public:
	Board();
	bool checkMove(int row, int col);
	void makeMove(int row, int col, char player);
	void printBoard();

private:
	char m_board[3][3] = {};
};